﻿Utstämplingsprogram

Angen sökvägen till programmet i inställningar för klockan i Garp.
"Aktivera exterrnt utstämplingsprogram"

Om utstämplngen sker på sista operation och antal är rapporterat startar kalkyldokument

config-filen skall ligga i samma mapp som programmet
Om "Test" anges till true visas dialogrutor och eventuella felmeddelanden

Ange fält för nya kalkylerade värdet och rapportnummer som skall köras

Fältet som används skall matcha fältet som uppdateras av dokumentet

Version 1.0