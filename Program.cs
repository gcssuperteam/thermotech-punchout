﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Garp;

namespace thermotech_punchout
{
    static class Program
    {
        static Garp.Application GarpUpdate;
        static ITable OGO, OGR, AGK;
        static ITabField AGK_newVIP;
        static string CurrentOrder, CurrentRow, CurrentOpn;
        static double CurrentQty;

        static void Main(string[] args)
        {
            try
            {
                CurrentOrder = args[0].PadRight(6);
                CurrentRow = args[1].PadLeft(3);
                CurrentOpn = args[2];
                CurrentQty = double.Parse(args[3].Replace(".", ","));

                if (Properties.Settings.Default.Test.ToLower() == "true")
                {
                    MessageBox.Show(CurrentOrder + "-" + CurrentRow + " ant:" + CurrentQty);
                }

                if (CurrentQty > 0)
                {
                    RunReport();
                }
            }
            catch (Exception e)
            {
                if (Properties.Settings.Default.Test.ToLower() == "true")
                {
                    MessageBox.Show("Error " + e);
                }
            }
        }

        static void RunReport()
        {
            try
            {
                Garp.Application GarpUpdate = new Garp.Application();

                OGO = GarpUpdate.Tables.Item("OGO");    // OrderOperation
                OGR = GarpUpdate.Tables.Item("OGR");    // OrderRad
                AGK = GarpUpdate.Tables.Item("AGK");    // ArtikelKalkyl

                OGO.Find(CurrentOrder + CurrentRow);
                OGO.Next();
                string lastOP = "000";

                while (OGO.Eof != true)
                {
                    if (OGO.Fields.Item("ONR").Value != CurrentOrder || OGO.Fields.Item("RDC").Value != CurrentRow)
                    {
                        break;
                    }
                    lastOP = OGO.Fields.Item("OPN").Value;
                    OGO.Next();
                }
                if (CurrentOpn == lastOP)
                {
                    // Val av fält för tillfälligt VIP-pris som skall levereras in till lagret P51 = kalkylerat standardpris
                    string tmpVIP = Properties.Settings.Default.FältKalkyleratPris;
                    AGK_newVIP = AGK.Fields.Item(tmpVIP);

                    string report = Properties.Settings.Default.DokKalkylering;

                    OGR.Find(CurrentOrder + CurrentRow);
                    string item = OGR.Fields.Item("ANR").Value;
                    IReport RunReport = GarpUpdate.ReportGenerators.Item("145").Reports.Item(report);
                    RunReport.RangeFrom = item;
                    RunReport.RangeTo = item;
                    RunReport.Medium = "0";
                    RunReport.Run();
                    RunReport.Wait();

                    AGK.Find(item);

                    if (Properties.Settings.Default.Test.ToLower() == "true")
                    {
                        MessageBox.Show(
                            "New value: " + AGK_newVIP.Value + ", Old value: " + OGR.Fields.Item("LVP").Value + Environment.NewLine +
                            "Price orderrow: " + OGR.Fields.Item("PRI").Value + " -> " + AGK_newVIP.Value
                            );
                    }

                    if (double.Parse(AGK_newVIP.Value.Replace(".", ",")) != 0 && AGK_newVIP.Value != OGR.Fields.Item("LVP").Value)
                    {
                        OGR.Fields.Item("LVP").Value = AGK_newVIP.Value;
                        OGR.Fields.Item("LPF").Value = "F";
                        OGR.Post();
                    }
                }
            }
            catch (Exception e)
            {
                if (Properties.Settings.Default.Test.ToLower() == "true")
                {
                    MessageBox.Show("Error " + e);
                }
            }
            OGO = null;
            OGR = null;
            AGK = null;
            GarpUpdate = null;
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
